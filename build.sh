#!/bin/bash -e

printf "Running tests...\n\n"

./vendor/bin/phpunit

printf "Starting server...\n\n"

php -S localhost:8000 -t public

printf "Ready to go!"
