# Line Server Problem
## How does your system work? (if not addressed in comments in source)

First of all, the following steps have to be done:  
1) Run the command `sh run.sh [number_of_lines]` to compose the txt file to be processed, where `[number_of_lines]` is the number of lines to write in the file.  
2) Run the command `sh build.sh` to run php unit testing and start the local server.

Now you will be able to call the API using the GET HTTP method:  
- `http://localhost[:port]/lines/[line_number]`, where `[port]` is the server port defined after running build.sh, and `[line_number]` the number of the line you want to search in the file.

When the GET /lines/ is called, the LineController class will be asked (by the web/route.php file) to process the `show($lineNumber)` service.
This service will read the text file with the help of the FileReader class to find and return, in the most possible performance way, the selected lineNumber.

GET /lines/ will return a JSON response with the status code 200 if the lineNumber is found or status code 413 if the lineNumber is beyond the end of the file.

## How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

I used [Jmeter](https://jmeter.apache.org/) for the performance tests, using a 4.5GB and 8.5GB file.   
(You can find my Jmeter configuration file in /tests/performance/HTTPRequest.jmx)  

The system will perform O(log N), because time goes up linearly while the n (number of lines) goes up exponentially: The bigger the "n", the bigger processing time will takes.

## How will your system perform with 100 users? 10000 users? 1000000 users?

In /tests/performance directory there are also three graphs that display testing results for 100, 500 and 1000 users.  
It's clear that the more users using the system, the more complexity will be the performance of it.  
Note: It's important to take in consideration that the performance tests were run in a local environment.

## What documentation, websites, papers, etc did you consult in doing this assignment?

- Remembering good unit testing practices: https://phpunit.de/manual/6.5/en/testing-practices.html#testing-practices.during-development  
- Copy/paste a CLI progress bar function just to put the FileGenerator more user-friendly: https://stackoverflow.com/questions/2124195/command-line-progress-bar-in-php  
- Remembering PHP fwrite function and the mode "w+": http://php.net/manual/en/function.fwrite.php
- Deal with large number of test users in Jmeter: https://www.blazemeter.com/blog/what%E2%80%99s-the-max-number-of-users-you-can-test-on-jmeter
- A very good input about the generator function yield: https://www.geekality.net/2016/02/28/php-stream-a-file-line-by-line-using-a-generator/

## What third-party libraries or other tools does the system use? How did you choose each library or framework you used?

- Jmeter: As I already referred, Jmeter (opensource) was very important to run tests of performance and understand how the system does with large amount of data / users.  
- Lumen/Laravel: Lumen is a minimalist PHP framework most used to create Rest API's. It was my choice because I'm comfortable with it and, consequently, my code can be more transparent.  

## How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?

I spent around 6 hours programming + 4 hours researching + 2 hours for the performance tests.  
If I had unlimited time:
1) I would like to spend it researching about run the system in a more efficient server to get faster responses.  
2) I would complete this test using Ruby.  
3) There are a few unit tests that could be improved later.

## If you were to critique your code, what would you have to say about it?

The unit tests are consulting the main file.txt. The best way to test data in this case could be adding mocks to simulate. If the file.txt is huge, that can take tests too long.  
 

