<?php

namespace App\Console\Commands;

use App\FileReader;
use Exception;
use Illuminate\Console\Command;

class FileGenerator extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "generate:file {number_of_lines}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates a new text file";

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $numberOfLines = $this->argument('number_of_lines');

            if ($numberOfLines < 1) {
                $this->error("Please select more than 1 number_of_lines");
                return;
            }

            $filePath = 'public/' . FileReader::FILENAME;

            if (file_exists($filePath)) unlink($filePath);

            $handleFile = fopen($filePath, 'w+');
            for($line = 1; $line <= $numberOfLines; $line++) {
                $content = "$line - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.";
                if ($line < $numberOfLines) $content .= PHP_EOL;
                fwrite($handleFile, $content);
                $this->progressBar($line, $numberOfLines);
            }
            fclose($handleFile);

            $this->info(' File was successfully created!');

        } catch (Exception $e) {
            $this->error("An error occurred while creating the text file");
        }
    }

    /**
     * Progress bar function from: https://stackoverflow.com/questions/2124195/command-line-progress-bar-in-php
     *
     * @param $done
     * @param $total
     */
    private function progressBar($done, $total) {
        $perc = floor(($done / $total) * 100);
        $left = 100 - $perc;
        $write = sprintf("\033[0G\033[2K[%'={$perc}s>%-{$left}s] - $perc%% - $done/$total", "", "");
        fwrite(STDERR, $write);
    }
}
