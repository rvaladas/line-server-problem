<?php

namespace App\Http\Controllers;

use App\FileReader;
use Illuminate\Http\JsonResponse;

class LineController extends Controller
{
    /**
     * Retrieve the line for the given line number.
     *
     * @param int $lineNumber
     * @return JsonResponse
     * @throws \Exception
     */
    public function show($lineNumber)
    {
        $file = new FileReader( base_path() . '/public/' . FileReader::FILENAME);
        $iterator = $file->iterate();

        $count = 1;
        foreach ($iterator as $item) {
            if ($count == $lineNumber) {
                return response()->json(['lineText' => $item]);
            }
            $count++;
        }

        return response()->json(null, 413);
    }
}
