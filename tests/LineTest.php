<?php

class LineTest extends TestCase
{
    protected $filePath;
    protected $file;

    /**
     * SetUp method
     */
    public function setUp()
    {
        parent::setUp();
        $this->filePath = base_path() . '/public/' . \App\FileReader::FILENAME;
        $this->file = new \App\FileReader($this->filePath);
    }

    /**
     * Expected result: Should get the same JSON response
     * @test
     */
    public function testItCanGetJsonEquals()
    {
        $this->json('GET', '/lines/1')->seeJsonEquals([
            "lineText" => "1 - Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.\n"
        ]);
    }

    /**
     * Expected result: Should get 200 status code
     * $lastLine is a valid line
     * @test
     */
    public function testItCanGet200StatusCode()
    {
        $lastLine = $this->file->getLastLine();
        $this->json('GET', '/lines/' . $lastLine)->seeStatusCode(200);
    }

    /**
     * Expected result: Should get 413 status code
     * $lineAfterLast is not a valid line
     * @test
     */
    public function testItCanGet413StatusCode()
    {
        $lineAfterLast = $this->file->getLastLine() + 2;
        $this->json('GET', '/lines/' . $lineAfterLast)->seeStatusCode(413);
    }

    /**
     * Expected result: Should find the file
     * @test
     */
    public function testIfFileExists()
    {
        $this->assertFileExists($this->filePath);
    }
}
