#!/bin/bash -e

re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
   echo "Error: Your input data is not a valid number" >&2; exit 1
fi

number_of_lines="$1"
printf "Processing new text file...\n"
php artisan generate:file $1
